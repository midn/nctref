#ifndef NCTREF_LEXER_H
#define NCTREF_LEXER_H

#include<stdio.h>

extern char *TOKEN_NAMES[];

typedef enum {
	TOKEN_IDENTIFIER,
	TOKEN_LOCAL,
	TOKEN_EOF,
	TOKEN_NUMBER,
	TOKEN_SEMICOLON,
	TOKEN_COLON,
	TOKEN_IF,
	TOKEN_PAREN_L,
	TOKEN_PAREN_R,
	TOKEN_SQUIGGLY_L,
	TOKEN_SQUIGGLY_R,
	TOKEN_EQUALS,
	TOKEN_PLUS,
	TOKEN_MINUS,
	TOKEN_STAR,
	TOKEN_SLASH,
	TOKEN_EXTERN,
	TOKEN_LOOP,
	TOKEN_BREAK,
	TOKEN_COMMA,
	TOKEN_AMPERSAND,
	TOKEN_VERTICAL_BAR,
	TOKEN_CARET,
	TOKEN_TILDE,
	TOKEN_DOUBLE_EQUALS,
	TOKEN_SQUAREN_L,
	TOKEN_SQUAREN_R,
	TOKEN_QUESTION_MARK,
	TOKEN_STRING
} TokenKind;

typedef struct {
	TokenKind type;
	int row, column;
	
	char *content; /* NULL for keywords. */
	size_t length; /* Not valid for everything. */
} Token;

Token nct_tokenize(FILE*);
Token *nct_lex(FILE*);

#endif
