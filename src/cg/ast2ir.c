#include"cg/ast2ir.h"
#include"ir.h"
#include"vreg.h"
#include<stdlib.h>
#include<string.h>

#ifndef __GNUC__
#define __thread
#endif

/* This was not written in an object-oriented manner. This will screw up parallelization which is in future plans. */

static char *data = NULL;
static size_t dataLen, dataCapacity;
static size_t totalThings;
static void *nextThing(char tag, size_t l) {
	if(dataCapacity - dataLen < l + 1) {
		data = realloc(data, dataCapacity += 1024);
	}
	char *ret = data + dataLen;
	*ret = tag;
	ret++;
	dataLen += l + 1;
	totalThings++;
	return ret;
}

static __thread int *degcomparatorud;
static int degcomparator(const void *a, const void *b) {
	return degcomparatorud[*(int*) a] - degcomparatorud[*(int*) b];
}

static void coloring_pass() {
	VRegTable *tbl = (VRegTable*) data;
	
	/* List of sorted pairs (a, b) where a < b of adjacent nodes (vreg IDs) */
	int adjacencyCount = 0;
	int *adjacencyList = malloc(0);
	
	int *degrees = malloc(sizeof(*degrees) * tbl->allocated);
	for(int i = 0; i < tbl->allocated; i++) {
		int degree = 0;
		
		for(int j = i + 1; j < tbl->allocated; j++) {
			/* Line intersection test (FIXME: Did I correctly put strict and unstrict inequalities? */
			if((tbl->array[i].liveStart >= tbl->array[j].liveStart && tbl->array[i].liveStart < tbl->array[j].liveEnd)
				|| (tbl->array[i].liveEnd > tbl->array[j].liveStart && tbl->array[i].liveEnd <= tbl->array[j].liveEnd)
				|| (tbl->array[j].liveStart >= tbl->array[i].liveStart && tbl->array[j].liveStart < tbl->array[i].liveEnd)
				|| (tbl->array[j].liveEnd > tbl->array[i].liveStart && tbl->array[j].liveEnd <= tbl->array[i].liveEnd)) {
				
				adjacencyList = realloc(adjacencyList, sizeof(*adjacencyList) * (adjacencyCount + 1) * 2);
				adjacencyList[adjacencyCount * 2 + 0] = i < j ? i : j;
				adjacencyList[adjacencyCount * 2 + 1] = i < j ? j : i;
				adjacencyCount++;
				
				degree++;
			}
		}
		
		degrees[i] = degree;
	}
	
	/* Welsh-Powell algorithm */
	
	/* Sort by degrees in descending order */
	int *vregsSortedByDegrees = malloc(sizeof(*vregsSortedByDegrees) * tbl->allocated);
	for(int i = 0; i < tbl->allocated; i++) vregsSortedByDegrees[i] = i;
	degcomparatorud = degrees;
	qsort(vregsSortedByDegrees, tbl->allocated, sizeof(*vregsSortedByDegrees), &degcomparator);
	
	/* Greedy coloring on sorted list */
	for(int i = 0; i < tbl->allocated; i++) {
		/* welsh plow my ass */
		int c;
		for(c = 0; c < tbl->allocated; c++) {
			for(int j = 0; j < tbl->allocated; j++) {
				if(tbl->array[j].color == c) {
					for(int a = 0; a < adjacencyCount; a++) {
						if((adjacencyList[a * 2 + 0] == ((i < j) ? i : j)) && (adjacencyList[a * 2 + 1] == ((i < j) ? j : i))) {
							goto nextColor;
						}
					}
				}
			}
			break;
			nextColor:;
		}
		tbl->array[i].color = c;
	}
}

static int updateLiveEnd(int vreg) {
	((VRegTable*) data)->array[vreg].liveEnd = totalThings;
	return vreg;
}

static int rstb_expr(int dst, AST *a) {
	if(a->nodeKind == AST_EXPRESSION_VAR) { /* a */
		if(a->expressionVar.thing->kind == VARTABLEENTRY_VAR) {
			int src = a->expressionVar.thing->data.var.vreg;
			
			if(dst == -1) {
				updateLiveEnd(src);
				return src;
			}
			
			XIReg2Reg *op = nextThing(XI_REG_2_REG, sizeof(*op));
			op->dst = dst;
			op->src = src;
			
			updateLiveEnd(dst);
			updateLiveEnd(src);
			
			return dst;
		} else if(a->expressionVar.thing->kind == VARTABLEENTRY_SYMBOL) {
			if(dst == -1) {
				dst = vreg_alloc((VRegTable*) data, type_size(a->expressionVar.type));
				((VRegTable*) data)->array[dst].liveStart = totalThings;
			}
			
			XISymbol2Reg *op = nextThing(XI_SYMBOL_2_REG, sizeof(*op));
			op->dst = dst;
			op->src = a->expressionVar.thing->data.symbol.linkName;
			op->deref = a->expressionVar.type->type == TYPE_TYPE_FUNCTION ? 0 : 2;
			
			updateLiveEnd(dst);
			
			return dst;
		}
	} else if(a->nodeKind == AST_EXPRESSION_UNARY_OP && a->expressionUnaryOp.operator == UNOP_REF && a->expressionUnaryOp.chaiuld->nodeKind == AST_EXPRESSION_VAR && a->expressionUnaryOp.chaiuld->expressionVar.thing->kind == VARTABLEENTRY_SYMBOL) {
		XISymbol2Reg *op = nextThing(XI_SYMBOL_2_REG, sizeof(*op));
		op->dst = dst;
		op->src = a->expressionUnaryOp.chaiuld->expressionVar.thing->data.symbol.linkName;
		op->deref = 0;
		
		updateLiveEnd(dst);
		
		return dst;
	} else if(a->nodeKind == AST_EXPRESSION_UNARY_OP && a->expressionUnaryOp.operator == UNOP_DEREF && a->expressionUnaryOp.chaiuld->nodeKind == AST_EXPRESSION_VAR) { /* *a */
		int src = a->expressionUnaryOp.chaiuld->expressionVar.thing->data.var.vreg;
		
		if(dst == -1) {
			dst = vreg_alloc((VRegTable*) data, type_size(a->expressionUnaryOp.chaiuld->expression.type->pointer.of));
			((VRegTable*) data)->array[dst].liveStart = totalThings;
		}
		
		XIReg2Reg *op = nextThing(XI_REG_2_REG, sizeof(*op));
		op->dst = dst;
		op->src = src;
		op->deref = 2;
		
		updateLiveEnd(dst);
		updateLiveEnd(src);
		
		return dst;
	} else if(a->nodeKind == AST_EXPRESSION_PRIMITIVE) {
		if(dst == -1) {
			dst = vreg_alloc((VRegTable*) data, 4);
			((VRegTable*) data)->array[dst].liveStart = totalThings;
		}
		
		XISetReg *op = nextThing(XI_SET_REG, sizeof(*op));
		op->deref = 0;
		op->reg = dst;
		op->val = a->expressionPrimitive.numerator;
		
		updateLiveEnd(dst);
		
		return dst;
	} else if(a->nodeKind == AST_EXPRESSION_BINARY_OP) {
		if(dst == -1) {
			dst = vreg_alloc((VRegTable*) data, 4);
			((VRegTable*) data)->array[dst].liveStart = totalThings;
		}
		
		rstb_expr(dst, a->expressionBinaryOp.operands[0]);
		
		for(int i = 1; i < a->expressionBinaryOp.amountOfOperands; i++) {
			AST *o = a->expressionBinaryOp.operands[i];
			
			if(o->nodeKind == AST_EXPRESSION_PRIMITIVE) {
				XIBinOpImm *op = nextThing(XI_BINOPIMM, sizeof(*op));
				op->op = XI_BINOP_ADD;
				op->regd = dst;
				op->imm = o->expressionPrimitive.numerator;
				op->deref = 0;
			} else {
				int regs = rstb_expr(-1, o);
				
				XIBinOp *op = nextThing(XI_BINOP, sizeof(*op));
				op->op = XI_BINOP_ADD;
				op->regd = dst;
				op->regs = regs;
				op->deref = 0;
				
				updateLiveEnd(regs);
			}
		}
		
		updateLiveEnd(dst);
		
		return dst;
	} else if(a->nodeKind == AST_EXPRESSION_CAST) {
		if(dst == -1) {
			dst = vreg_alloc((VRegTable*) data, 4);
			((VRegTable*) data)->array[dst].liveStart = totalThings;
		}
		rstb_expr(dst, a->expressionCast.what);
		
		if(a->expressionCast.what->nodeKind == AST_EXPRESSION_PRIMITIVE && a->expressionCast.to->type == TYPE_TYPE_POINTER) {
			/* Do nothing. */
		} else if(a->expressionCast.what->expression.type->type == TYPE_TYPE_POINTER && a->expressionCast.to->type == TYPE_TYPE_POINTER) {
			/* Do nothing. */
		} else abort();
	} else if(a->nodeKind == AST_EXPRESSION_CALL) {
		if(dst == -1) {
			dst = vreg_alloc((VRegTable*) data, 4);
			((VRegTable*) data)->array[dst].liveStart = totalThings;
		}
		
		int callee = rstb_expr(-1, a->expressionCall.what);
		
		int argCount = a->expressionCall.what->expression.type->function.argCount;
		XICallReg *op = nextThing(XI_CALL, sizeof(*op) + sizeof(op->stackRegs[0]) * argCount);
		op->stackRegsCount = argCount;
		for(int i = 0; i < argCount; i++) {
			op->stackRegs[i] = rstb_expr(-1, a->expressionCall.args[i]);
		}
		op->deref = 0;
		op->reg = callee;
		op->retReg = dst;
	} else abort();
}

static void serializeconst(char *dest, AST *a) {
	if(a->nodeKind == AST_EXPRESSION_ARRAY) {
		char *ptr = dest;
		for(int i = 0; i < a->expressionArray.type->array.length; i++) {
			serializeconst(ptr, a->expressionArray.items[i]);
			ptr += type_size(a->expressionArray.type->array.of);
		}
	} else if(a->nodeKind == AST_EXPRESSION_PRIMITIVE) {
		memcpy(dest, &a->expressionPrimitive.numerator, type_size(a->expressionPrimitive.type));
	} else abort();
}

static void rstb_chunk(ASTChunk *a, int isTopLevel) {
	static size_t nextLocalId = 0;
	
	/* Stack */
	static __thread int loopLabelCont[64];
	static __thread int loopLabelBreak[64];
	static __thread int loopLabelDepth = 0;
	
	AST *s = a->statements;
	while(s) {
		if(s->nodeKind == AST_STATEMENT_DECL) {
			if(s->statementDecl.thing->kind == VARTABLEENTRY_VAR) {
				s->statementDecl.thing->data.var.vreg = vreg_alloc((VRegTable*) data, type_size(s->statementDecl.thing->type));
				
				VReg *v = &((VRegTable*) data)->array[s->statementDecl.thing->data.var.vreg];
				v->liveStart = totalThings;
				v->liveEnd = totalThings;
				
				if(s->statementDecl.expression) {
					rstb_expr(s->statementDecl.thing->data.var.vreg, s->statementDecl.expression);
				}
			} else if(s->statementDecl.thing->kind == VARTABLEENTRY_SYMBOL) {
				XIMarkName *op0 = nextThing(XI_MARK_NAME, sizeof(*op0));
				op0->isLocal = s->statementDecl.thing->data.symbol.isLocal;
				op0->isExtern = s->statementDecl.thing->data.symbol.isExternal;
				op0->name = s->statementDecl.thing->data.symbol.linkName;
				
				if(!op0->isExtern) {
					uint16_t len = type_size(s->statementDecl.thing->type);
					XIDirect *op1 = nextThing(XI_DIRECT, sizeof(*op1) + len);
					op1->length = len;
					
					serializeconst(op1->data, s->statementDecl.expression);
				}
			}
		} else if(s->nodeKind == AST_STATEMENT_ASSIGN) {
			/* *var = prim; */
			if(s->statementAssign.what->nodeKind == AST_EXPRESSION_UNARY_OP && s->statementAssign.what->expressionUnaryOp.operator == UNOP_DEREF && s->statementAssign.what->expressionUnaryOp.chaiuld->nodeKind == AST_EXPRESSION_VAR && s->statementAssign.what->expressionUnaryOp.chaiuld->expressionVar.thing->kind == VARTABLEENTRY_VAR && s->statementAssign.to->nodeKind == AST_EXPRESSION_PRIMITIVE) {
				XISetReg *op = nextThing(XI_SET_REG, sizeof(*op));
				op->deref = 1;
				op->derefSz = type_size(s->statementAssign.what->expressionUnaryOp.chaiuld->expression.type->pointer.of);
				op->reg = s->statementAssign.what->expressionUnaryOp.chaiuld->expressionVar.thing->data.var.vreg;
				op->val = s->statementAssign.to->expressionPrimitive.numerator;
			} else {
				if(s->statementAssign.what->nodeKind == AST_EXPRESSION_UNARY_OP && s->statementAssign.what->expressionUnaryOp.operator == UNOP_DEREF) {
					int dstref = rstb_expr(-1, s->statementAssign.what->expressionUnaryOp.chaiuld);
					int src = rstb_expr(-1, s->statementAssign.to);
					
					XIReg2Reg *op = nextThing(XI_REG_2_REG, sizeof(*op));
					op->deref = 1;
					op->derefSz = type_size(s->statementAssign.what->expressionUnaryOp.chaiuld->expression.type->pointer.of);
					op->src = src;
					op->dst = dstref;
					
					updateLiveEnd(dstref);
					updateLiveEnd(src);
				} else {
					rstb_expr(rstb_expr(-1, s->statementAssign.what), s->statementAssign.to);
				}
			}
		} else if(s->nodeKind == AST_STATEMENT_EXT_ALIGN) {
			XIAlign *op = nextThing(XI_ALIGN, sizeof(*op));
			op->val = s->statementExtAlign.val;
		} else if(s->nodeKind == AST_STATEMENT_LOOP) {
			loopLabelCont[loopLabelDepth] = nextLocalId++;
			loopLabelBreak[loopLabelDepth] = nextLocalId++;
			
			XIMarkLocal *op0 = nextThing(XI_MARK_LOCAL, sizeof(*op0));
			op0->id = loopLabelCont[loopLabelDepth];
			
			loopLabelDepth++;
			rstb_chunk(s->statementLoop.body, 0);
			loopLabelDepth--;
			
			XIJumpLocal *op1 = nextThing(XI_JUMP_LOCAL, sizeof(*op1));
			op1->cond = XI_JUMPCOND_ALWAYS;
			op1->id = op0->id;
			
			XIMarkLocal *op2 = nextThing(XI_MARK_LOCAL, sizeof(*op2));
			op2->id = loopLabelBreak[loopLabelDepth];
		} else if(s->nodeKind == AST_STATEMENT_IF) {
			size_t id = nextLocalId++;
			
			if(s->statementIf.expression->nodeKind == AST_EXPRESSION_BINARY_OP && s->statementIf.expression->expressionBinaryOp.operators[0] == BINOP_EQUAL) {
				int r1 = rstb_expr(-1, s->statementIf.expression->expressionBinaryOp.operands[0]);
				if(s->statementIf.expression->expressionBinaryOp.operands[1]->nodeKind == AST_EXPRESSION_PRIMITIVE) {
					XICmpImm *op0 = nextThing(XI_CMP_IMM, sizeof(*op0));
					op0->dst = r1;
					op0->src = s->statementIf.expression->expressionBinaryOp.operands[1]->expressionPrimitive.numerator;
					op0->deref = 0;
				} else {
					int r2 = rstb_expr(-1, s->statementIf.expression->expressionBinaryOp.operands[1]);
					
					XICmpReg *op0 = nextThing(XI_CMP_REG, sizeof(*op0));
					op0->dst = r1;
					op0->src = r2;
					op0->deref = 0;
					
					updateLiveEnd(r2);
				}
				
				updateLiveEnd(r1);
				
				XIJumpLocal *op1 = nextThing(XI_JUMP_LOCAL, sizeof(*op1));
				op1->cond = XI_JUMPCOND_NOTEQUAL;
				op1->id = id;
			} else abort();
			
			rstb_chunk(s->statementIf.then, 0);
			
			XIMarkLocal *op1 = nextThing(XI_MARK_LOCAL, sizeof(*op1));
			op1->id = id;
		} else if(s->nodeKind == AST_STATEMENT_BREAK) {
			XIJumpLocal *op = nextThing(XI_JUMP_LOCAL, sizeof(*op));
			op->cond = XI_JUMPCOND_ALWAYS;
			op->id = loopLabelBreak[loopLabelDepth - 1];
		} else if(s->nodeKind == AST_STATEMENT_EXT_ORG) {
			XIOrg *op = nextThing(XI_ORG, sizeof(*op));
			op->val = s->statementExtOrg.val;
		} else if(s->nodeKind == AST_STATEMENT_EXPR) {
			rstb_expr(-1, s->statementExpr.expr);
		}
		
		s = s->statement.next;
	}
	
	if(isTopLevel) {
		nextThing(XI_END, 0);
	}
}

void *reestablish(AST *top) {
	data = malloc(dataLen = dataCapacity = sizeof(VRegTable));
	vreg_tbl_new((VRegTable*) data);
	
	rstb_chunk(&top->chunk, 1);
	coloring_pass(&top->chunk);
	
	return data;
}