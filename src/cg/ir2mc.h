#ifndef CG_X86_H
#define CG_X86_H

#include"dstr.h"
#include"ast.h"

typedef enum {
	X86_TARGET_80386, X86_TARGET_8086, X86_TARGET_M
} X86Target;

// Used to determine how to interact with SP, IP.
typedef enum {
	X86_MODE_UNSELECTED, X86_MODE_16, X86_MODE_32, X86_MODE_64
} X86Mode;

typedef struct {
	dstr text;
	
	size_t lidx; // ID of next .L label
	
	size_t loopStack[64];
	size_t loopStackIndex;
	
	X86Target target;
	X86Mode mode;
} X86;

void x86_new(X86*);

void x86_cg(X86*, void*);

void x86_finish(X86*);

#endif
