#include"ir2mc.h"

#include<assert.h>
#include<stdlib.h>
#include"vreg.h"
#include"ir.h"
#include<string.h>

#ifndef __GNUC__
int __builtin_ctz(int i) {
	switch(i) {
		case 1: return 0;
		case 2: return 1;
		case 4: return 2;
		case 8: return 3;
		case 16: return 4;
	}
}
#endif

static const char *const registerSet[] = {
	"al", "bl", "cl", "dl",
	"ax", "bx", "cx", "dx",
	"eax", "ebx", "ecx", "edx",
	"rax", "rbx", "rcx", "rdx",
};

static int reg_cast_size(int r, int s) {
	return __builtin_ctz(s) * 4 + r % 4;
}

static int get_reg_size(int r) {
	return 1 << (r / 4);
}

static const char *size_specifier(int size) {
	static const char *const rets[] = {
#ifdef SYNTAX_GAS
		"b", "w", "l", "q"
#else
		"byte", "word", "dword", "qword"
#endif
	};
	return rets[__builtin_ctz(size)];
}

static dstr describe_vreg(VReg *v, int size) {
	if(size == -1) size = v->size;
	
#ifdef SYNTAX_GAS
	return dstrfmt(dstrempty(), "%%%s", registerSet[__builtin_ctz(size) * 4 + v->color]);
#else
	return dstrfmt(dstrempty(), "%s", registerSet[__builtin_ctz(size) * 4 + v->color]);
#endif
}

void x86_new(X86 *X) {
	X->text = dstrempty();
	X->lidx = 0;
	X->loopStackIndex = 0;
	
	X->target = X86_TARGET_80386;
	X->mode = X86_MODE_32;
}

void x86_cg(X86 *X, void *fn) {
	VRegTable *tbl = fn;
	char *data = (char*) fn + sizeof(*tbl);
	
	while(1) {
		if(*data == XI_END) {
			break;
		} else if(*data == XI_SET_REG) {
			XISetReg *op = (XISetReg*) ++data;
			dstr d = describe_vreg(&tbl->array[op->reg], -1);
			const char *szspec = size_specifier(op->deref ? op->derefSz : tbl->array[op->reg].size);
#ifdef SYNTAX_GAS
			X->text = dstrfmt(X->text, "mov%s $%i, %c%S%c\n", szspec, op->val, op->deref ? '(' : 0, d, op->deref ? '(' : 0);
#else
			X->text = dstrfmt(X->text, "mov %s %c%S%c, %i\n", szspec, op->deref ? '[' : 0, d, op->deref ? ']' : 0, op->val);
#endif
			dstrfree(d);
			data += sizeof(*op);
		} else if(*data == XI_REG_2_REG) {
			XIReg2Reg *op = (XIReg2Reg*) ++data;
			
			dstr s = describe_vreg(&tbl->array[op->src], -1), d = describe_vreg(&tbl->array[op->dst], -1);
			
			if(strcmp(s, d)) { /* string comparison is correct technically but not very elegant */
#ifdef SYNTAX_GAS
				X->text = dstrfmt(X->text, "mov %c%S%c, %c%S%c\n", op->deref == 2 ? '(' : 0, s, op->deref == 2 ? ')' : 0, op->deref == 1 ? '(' : 0, d, op->deref == 1 ? ')' : 0);
#else
				X->text = dstrfmt(X->text, "mov %c%S%c, %c%S%c\n", op->deref == 1 ? '[' : 0, d, op->deref == 1 ? ']' : 0, op->deref == 2 ? '[' : 0, s, op->deref == 2 ? ']' : 0);
#endif
			}
			
			dstrfree(s);
			dstrfree(d);
			
			data += sizeof(*op);
		} else if(*data == XI_BINOP) {
			XIBinOp *op = (XIBinOp*) ++data;
			
			dstr d = describe_vreg(&tbl->array[op->regd], -1);
			dstr s = describe_vreg(&tbl->array[op->regs], tbl->array[op->regd].size);
			
#ifdef SYNTAX_GAS
			X->text = dstrfmt(X->text, "add %c%S%c, %c%S%c\n", op->deref == 2 ? '(' : 0, s, op->deref == 2 ? ')' : 0, op->deref == 1 ? '(' : 0, d, op->deref == 1 ? ')' : 0);
#else
			X->text = dstrfmt(X->text, "add %c%S%c, %c%S%c\n", op->deref == 1 ? '[' : 0, d, op->deref == 1 ? ']' : 0, op->deref == 2 ? '[' : 0, s, op->deref == 2 ? ']' : 0);
#endif
			
			dstrfree(s);
			dstrfree(d);
			
			data += sizeof(*op);
		} else if(*data == XI_BINOPIMM) {
			XIBinOpImm *op = (XIBinOpImm*) ++data;
			
			dstr d = describe_vreg(&tbl->array[op->regd], -1);
			
			if(op->op == XI_BINOP_ADD && op->imm == 1) {
#ifdef SYNTAX_GAS
				X->text = dstrfmt(X->text, "inc %c%S%c\n", op->deref == 1 ? '(' : 0, d, op->deref == 1 ? ')' : 0);
#else
				X->text = dstrfmt(X->text, "inc %c%S%c\n", op->deref == 1 ? '[' : 0, d, op->deref == 1 ? ']' : 0);
#endif
			} else {
#ifdef SYNTAX_GAS
				X->text = dstrfmt(X->text, "add %c%i, %c%S%c\n", op->deref == 2 ? '$' : 0, op->imm, op->deref == 1 ? '(' : 0, d, op->deref == 1 ? ')' : 0);
#else
				X->text = dstrfmt(X->text, "add %c%S%c, %c%i%c\n", op->deref == 1 ? '[' : 0, d, op->deref == 1 ? ']' : 0, op->deref == 2 ? '[' : 0, op->imm, op->deref == 2 ? ']' : 0);
#endif
			}
			
			dstrfree(d);
			
			data += sizeof(*op);
		} else if(*data == XI_ALIGN) {
			XIAlign *op = (XIAlign*) ++data;
			
#ifdef SYNTAX_GAS
			X->text = dstrfmt(X->text, ".align %i\n", op->val);
#else
			X->text = dstrfmt(X->text, "resb ($-$$+%i)/%i*%i-$+$$\n", op->val - 1, op->val, op->val);
#endif
			
			data += sizeof(*op);
		} else if(*data == XI_DIRECT) {
			XIDirect *op = (XIDirect*) ++data;
			
#ifdef SYNTAX_GAS
			X->text = dstrfmt(X->text, ".byte ");
#else
			X->text = dstrfmt(X->text, "db ");
#endif
			
			for(uint16_t i = 0; i < op->length; i++) {
				X->text = dstrfmt(X->text, "%c%i", i == 0 ? 0 : ',', op->data[i]);
			}
			
			X->text = dstrfmt(X->text, "\n");
			
			data += sizeof(*op) + op->length;
		} else if(*data == XI_MARK_LOCAL) {
			XIMarkLocal *op = (XIMarkLocal*) ++data;
			
			X->text = dstrfmt(X->text, ".L%i:\n", op->id);
			
			data += sizeof(*op);
		} else if(*data == XI_JUMP_LOCAL) {
			XIJumpLocal *op = (XIJumpLocal*) ++data;
			
			static const char *const condStrings[] = {[XI_JUMPCOND_ALWAYS] = "mp", [XI_JUMPCOND_EQUAL] = "e", [XI_JUMPCOND_NOTEQUAL] = "ne"};
			
#ifdef SYNTAX_GAS
			X->text = dstrfmt(X->text, "j%s $.L%i\n", condStrings[op->cond], op->id);
#else
			X->text = dstrfmt(X->text, "j%s .L%i\n", condStrings[op->cond], op->id);
#endif
			
			data += sizeof(*op);
		} else if(*data == XI_CMP_REG) {
			XICmpReg *op = (XICmpReg*) ++data;
			
			dstr s = describe_vreg(&tbl->array[op->src], -1), d = describe_vreg(&tbl->array[op->dst], -1);
			
#ifdef SYNTAX_GAS
			X->text = dstrfmt(X->text, "cmp %c%S%c, %c%S%c\n", op->deref == 2 ? '(' : 0, s, op->deref == 2 ? ')' : 0, op->deref == 1 ? '(' : 0, d, op->deref == 1 ? ')' : 0);
#else
			X->text = dstrfmt(X->text, "cmp %c%S%c, %c%S%c\n", op->deref == 1 ? '[' : 0, d, op->deref == 1 ? ']' : 0, op->deref == 2 ? '[' : 0, s, op->deref == 2 ? ']' : 0);
#endif
			
			data += sizeof(*op);
		} else if(*data == XI_CMP_IMM) {
			XICmpImm *op = (XICmpImm*) ++data;
			
			dstr d = describe_vreg(&tbl->array[op->dst], -1);
			
#ifdef SYNTAX_GAS
			X->text = dstrfmt(X->text, "cmp %c%i%c, %c%S%c\n", op->deref == 2 ? '(' : 0, op->src, op->deref == 2 ? ')' : 0, op->deref == 1 ? '(' : 0, d, op->deref == 1 ? ')' : 0);
#else
			X->text = dstrfmt(X->text, "cmp %c%S%c, %c%i%c\n", op->deref == 1 ? '[' : 0, d, op->deref == 1 ? ']' : 0, op->deref == 2 ? '[' : 0, op->src, op->deref == 2 ? ']' : 0);
#endif
			
			data += sizeof(*op);
		} else if(*data == XI_SYMBOL_2_REG) {
			XISymbol2Reg *op = (XISymbol2Reg*) ++data;
			
			dstr d = describe_vreg(&tbl->array[op->dst], -1);
			
#ifdef SYNTAX_GAS
			X->text = dstrfmt(X->text, "mov %c%s%c, %c%S%c\n", op->deref == 2 ? '(' : 0, op->src, op->deref == 2 ? ')' : 0, op->deref == 1 ? '(' : 0, d, op->deref == 1 ? ')' : 0);
#else
			X->text = dstrfmt(X->text, "mov %c%S%c, %c%s%c\n", op->deref == 1 ? '[' : 0, d, op->deref == 1 ? ']' : 0, op->deref == 2 ? '[' : 0, op->src, op->deref == 2 ? ']' : 0);
#endif
			
			dstrfree(d);
			
			data += sizeof(*op);
		} else if(*data == XI_ORG) {
			XIOrg *op = (XIOrg*) ++data;
			
#ifdef SYNTAX_GAS
			X->text = dstrfmt(X->text, ".org %i\n", op->val);
#else
			X->text = dstrfmt(X->text, "org %i\n", op->val);
#endif
			
			data += sizeof(*op);
		} else if(*data == XI_MARK_NAME) {
			XIMarkName *op = (XIMarkName*) ++data;
			
			if(op->isExtern) {
#ifndef SYNTAX_GAS
				X->text = dstrfmt(X->text, "extern %s\n", op->name);
#endif
			} else {
				if(!op->isLocal) {
#ifdef SYNTAX_GAS
					X->text = dstrfmt(X->text, ".globl %s\n", op->name);
#else
					X->text = dstrfmt(X->text, "global %s\n", op->name);
#endif
				}
				
				X->text = dstrfmt(X->text, "%s:\n", op->name);
			}
			
			data += sizeof(*op);
		} else if(*data == XI_CALL) {
			XICallReg *op = (XICallReg*) ++data;
			
			dstr d = describe_vreg(&tbl->array[op->reg], -1);
			dstr r = describe_vreg(&tbl->array[op->retReg], -1);
			
#ifdef SYNTAX_GAS
#error not yet supported
#else
			X->text = dstrfmt(X->text, "push ecx\npush edx\n");
			for(int i = 0; i < op->stackRegsCount; i++) {
				dstr a = describe_vreg(&tbl->array[op->stackRegs[i]], 4);
				X->text = dstrfmt(X->text, "push %S\n", a);
				dstrfree(a);
			}
			X->text = dstrfmt(X->text, "call %c%s%c\nmov %S, %s\nadd esp, %i\n", op->deref == 1 ? '[' : 0, d, op->deref == 1 ? ']' : 0, r, registerSet[reg_cast_size(0, tbl->array[op->retReg].size)], 4 * op->stackRegsCount);
			X->text = dstrfmt(X->text, "pop edx\npop ecx\n");
#endif
			
			dstrfree(r);
			dstrfree(d);
			
			data += sizeof(*op) + sizeof(op->stackRegs[0]) * op->stackRegsCount;
		} else abort();
	}
}

void x86_finish(X86 *X) {
	fputs(X->text, stdout);
}