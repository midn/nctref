#include"vreg.h"

#include<stdlib.h>

void vreg_tbl_new(VRegTable *ret) {
	ret->allocated = 0;
	ret->array = malloc((ret->capacity = 4) * sizeof(*ret->array));
}

int vreg_alloc(VRegTable *tbl, int size) {
	if(tbl->allocated == tbl->capacity) {
		tbl->array = realloc(tbl->array, sizeof(*tbl->array) * (tbl->capacity += 8));
	}
	
	VReg *ret = &tbl->array[tbl->allocated];
	ret->color = -1;
	ret->size = size;
	return tbl->allocated++;
}