#ifndef H_X86_IR
#define H_X86_IR

#include<stdint.h>

#define XI_BINOP 0
#define XI_BINOPIMM 1

#define XI_BINOP_ADD 0
#define XI_BINOP_SUB 1
#define XI_BINOP_MUL 2

#define XI_UNOP 2
#define XI_UNOP_NEG 0
#define XI_UNOP_DEREF 1
#define XI_UNOP_NOP 2

#define XI_SET_REG 3
#define XI_SET_SYMBOL 4
#define XI_SET_MEMORY 5
#define XI_REG_2_SYMBOL 6
#define XI_REG_2_MEMORY 7
#define XI_REG_2_REG 8

#define XI_END 9

#define XI_ALIGN 10

#define XI_DIRECT 11

#define XI_JUMP_LOCAL 12
#define XI_MARK_LOCAL 13

#define XI_JUMPCOND_ALWAYS 0
#define XI_JUMPCOND_EQUAL 2
#define XI_JUMPCOND_NOTEQUAL 3

#define XI_CMP_REG 14
#define XI_CMP_IMM 15

#define XI_SYMBOL_2_REG 16

#define XI_ORG 17

#define XI_MARK_NAME 18

#define XI_CALL 19

typedef struct {
	int deref;
	int derefSz;
	int reg;
	uint64_t val;
} XISetReg;

typedef struct {
	const char *name;
	uint64_t val;
} XISetSymbol;

typedef struct {
	uint64_t addr;
	uint64_t val;
} XISetMemory;

typedef struct {
	int src;
	const char *dst;
} XIReg2Symbol;

typedef struct {
	int src;
	uint64_t dst;
} XIReg2Memory;

typedef struct {
	int deref; /* 0 = none, 1 = destination, 2 = source */
	int derefSz;
	int dst;
	int src;
} XIReg2Reg;

typedef struct {
	char op;
	int deref; /* 0 = none, 1 = destination, 2 = source */
	int derefSz;
	int regd;
	int regs;
} XIBinOp;

typedef struct {
	char op;
	int deref; /* 0 = none, 1 = destination, 2 = source */
	int derefSz;
	int regd;
	ssize_t imm;
} XIBinOpImm;

typedef struct {
	int reg;
} XIUnOp;

typedef struct {
	int val;
} XIAlign;

typedef struct {
	uint16_t length;
	char data[];
} XIDirect;

typedef struct {
	size_t id;
} XIMarkLocal;

typedef struct {
	size_t id;
	uint8_t cond;
} XIJumpLocal;

typedef struct {
	int deref; /* 0 = none, 1 = destinaion, 2 = source */
	int derefSz;
	int dst;
	int src;
} XICmpReg;

typedef struct {
	int deref; /* 0 = none, 1 = destinaion, 2 = source */
	int derefSz;
	int dst;
	ssize_t src;
} XICmpImm;

typedef struct {
	int deref;
	int dst;
	const char *src;
} XISymbol2Reg;

typedef struct {
	size_t val;
} XIOrg;

typedef struct {
	const char *name;
	int isLocal;
	int isExtern;
} XIMarkName;

typedef struct {
	int deref;
	int reg;
	
	int retReg;
	int stackRegsCount;
	int stackRegs[];
} XICallReg;

#endif