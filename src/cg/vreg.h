#ifndef _H_VREG
#define _H_VREG

#include<stddef.h>

typedef struct {
	int size;
	
	int color;
	int isSpilled;
	
	int liveStart;
	int liveEnd;
} VReg;

typedef struct {
	size_t allocated;
	size_t capacity;
	VReg *array;
} VRegTable;

void vreg_tbl_new(VRegTable*);
int vreg_alloc(VRegTable*, int size);

#endif